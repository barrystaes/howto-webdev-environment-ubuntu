# How-to WebDev environment
Checklist for a new Ubuntu 20.04 LTS system for development.

## Changelog:
- 20200617 BarryStaes: New, based on Ubuntu18 checklist.
- 20201110 BarryStaes: Updated.

## Checklist:

#### VirtualBox VM aanmaken

- 4 vCPU
- 6GB RAM
- 128MB Video
- 3D acceleration enabled
- HDD 120GB

#### Install Ubuntu 20.04 LTS

Minimal installation (geen bloatware) with updates. No 3th party needed.

#### Update OS

	$ sudo apt-get update
	#$ sudo apt-get dist-upgrade

#### Install VirtualBox Guest Additions

Insert CD via menu and run

	$ shutdown -r now
	
If you use a Shared Folder in VirtualBox you need this:
	
	$ sudo usermod -aG vboxsf $(whoami)

#### Fix DNS search domain on local network

If this works ok:

    $ nslookup foo.bar.local
    $ ping 198.51.100.123

And this does not:

    $ ping foo.bar.local

Then try (from https://askubuntu.com/questions/81797/nslookup-finds-ip-but-ping-doesnt )

    $ sudo apt-get remove libnss-mdns

#### Install tools

	$ sudo apt-get install curl git screen htop
	$ git --version
	git version 2.25.1
	
Set author + date format `2019-05-21 10:52:12 +0200`. (instead of `Mon May 20 16:04:52 2019 +0200`)
	
	$ git config --global user.name "Mona Lisa"
	$ git config --global user.email "email@example.com"
	$ git config --global log.date iso
	

#### Install JetBrains Toolbox

from https://www.jetbrains.com/toolbox/app/

	$ tar xf jetbrains-toolbox-1.17.7018.tar.gz
	$ sudo mv ./jetbrains-toolbox-1.17.7018 /opt/
	$ /opt/jetbrains-toolbox-1.17.7018/jetbrains-toolbox

Configure licence

#### Install JAVA JRE

	$ sudo apt-get install default-jre
	$ java -version
	openjdk version "11.0.9" 2020-10-20
	OpenJDK Runtime Environment (build 11.0.9+11-Ubuntu-0ubuntu1.20.04)
	OpenJDK 64-Bit Server VM (build 11.0.9+11-Ubuntu-0ubuntu1.20.04, mixed mode, sharing)



#### Install SmartGit

van https://www.syntevo.com/smartgit/

	$ tar xf smartgit-generic-20_1_2.tar.gz
	$ sudo mv ./smartgit /opt/
	$ /opt/smartgit/bin/smartgit.sh

Configure license.

#### Install GitLab server certificate

Open https://foo.bar.local in browser and save certificate to `.crt` file. Now install it:

    $ sudo mv ~/foo.bar.local.crt /usr/local/share/ca-certificates/
    $ sudo update-ca-certificates

Perhaps fix DNS so that foo.bar.local does resolve, see above.

#### Install Chromium

Voor minder Google bloat (maar nog steeds tracking) gebruik
 
    $ sudo apt install -y chromium-browser
    
of de echte van https://www.google.nl/chrome/

	$ sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) main universe restricted multiverse"
	$ sudo apt-get update
	$ sudo dpkg -i google-chrome-stable_current_amd64.deb

## Android development

Install Android Studio from JetBrains toolbox.

Emulation: if emulation for debug does not work (no nested vt-d) run Android straight on the host Virtualbox using https://www.osboxes.org/ and let Android Studio connect to it like any virtual (AVD) or real (USB) device.

    $ sudo apt install adb
    $ adb connect 11.22.33.44

## Web development

#### Install tools

	$ sudo apt-get update
	$ sudo apt-get install curl git screen
	$ git --version
	git version 2.17.1

#### Install nodejs LTS

from https://nodejs.org/en/download/current/ latest LTS

	$ curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
	$ sudo apt-get install -y nodejs
	$ node -v
	v14.15.3
	$ npm -v
	6.14.9

#### Install yarn

from https://yarnpkg.com/lang/en/docs/install/#linux-tab

	$ curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
	$ echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
	$ sudo apt-get update && sudo apt-get install yarn
	$ yarn --version
	1.22.5

#### Install Docker

	$ sudo apt-get update
	$ sudo apt-get install apt-transport-https ca-certificates software-properties-common
	$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    $ sudo apt-key fingerprint 0EBFCD88

Verify that the key fingerprint is `9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88`.

	$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
	$ sudo apt-get update
	$ sudo apt-get install docker-ce
	$ docker -v
	Docker version 19.03.13, build 4484c46d9d
	$ sudo docker run hello-world

Fix use docker without sudo

	$ sudo usermod -aG docker $USER
	$ shutdown -r now
	$ docker run hello-world

#### Install Docker-Compose

van https://docs.docker.com/compose/install/

	$ sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    $ sudo chmod +x /usr/local/bin/docker-compose
	$ docker-compose -v
	docker-compose version 1.27.4, build 40524192

Add docker commandline completion van https://docs.docker.com/compose/completion/

	$ sudo curl -L https://raw.githubusercontent.com/docker/compose/1.26.0/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose


## TODO test en documenteer hier

#### Install php en composer

van https://getcomposer.org/

	#$ sudo apt-get install php7.0-cli
	#$ php -v
	#$ composer


## Personal preference:

#### Diversen

- `$ mkdir ~/ProjectsGit`
- Configure WebStorm use Yarn instead of NPM:
  - click Settings | Languages & Frameworks | Node.js and NPM
  - click ellipsis button next to Node.js interpreter to open "Node.js interpreters" dialog
  - installed yarn packages will be available in drop-down list for your convenience.
- Create SSH key and add to my Gitlab profile


#### Install gedit dockerfile syntax

van https://github.com/ilogue/docker.lang

	$ sudo curl -o /usr/share/gtksourceview-4/language-specs/docker.lang https://github.com/ilogue/docker.lang/blob/master/docker.lang
